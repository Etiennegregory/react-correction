import React from "react";
import {
  Routes,
  Route,
} from "react-router-dom";
import {
    Home, 
    Articles,
    Article,
    Edit,
} from "./screens";



export default function App(){
    return(
        <Routes>
            <Route path="/" element={<Home/>}></Route>
            <Route path="articles" element={<Articles/>}></Route>
            <Route path="articles/:id" element={<Article/>}></Route>
            <Route path="edit/:id" element={<Edit/>}></Route>
        </Routes>
    )
}